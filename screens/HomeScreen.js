import React, {Component} from 'react';
import {
    View,
    StyleSheet,
    ActivityIndicator,
    FlatList, Text, Image
} from 'react-native';

class FlatListItem extends Component {
    render() {
        return (
            <View style={{
                flex: 1,
                flexDirection: 'column',
                paddingBottom: 1
            }}>

                <View style={{
                    flex: 1,
                    flexDirection: 'row',
                    backgroundColor: 'mediumseagreen'
                }}>
                    <Image
                        source={{uri: this.props.item.urlImg}}
                        style={{width: 100, height: 100, margin: 5}}
                    >
                    </Image>
                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <Text style={styleItem.flatListItem}>{this.props.item.name}</Text>
                        <Text style={styleItem.flatListItem}>{this.props.item.description}</Text>
                    </View>

                </View>
            </View>
        )
    }
}

const styleItem = StyleSheet.create({
    flatListItem: {
        color: 'white',
        padding: 10,
        fontSize: 16,
    }
});
export default class HomeScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            dataSource: [],
        }
    }

    componentDidMount() {
        return fetch("http://172.23.176.1:3131/venues")
            .then(response => response.json())
            .then(response => {

                    this.setState({
                        isLoading: false,
                        dataSource: response,
                    })
                }
            ).catch((error) => {
                console.log(error);
            })
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={styles.container}>
                    <ActivityIndicator/></View>
            )
        }
        else {

            return (
                <View style={{flex: 1}}>
                    <FlatList
                        data={this.state.dataSource}
                        renderItem={({item, index}) => {
                            return (
                                <FlatListItem item={item} index={index}>
                                </FlatListItem>
                            )
                        }}
                    >
                    </FlatList>
                </View>
            )

        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    item: {
        flex: 1,
        alignSelf: 'stretch',
        margin: 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#eee'
    }
});